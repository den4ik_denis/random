package com.RandomNumbers;

public class RandomNumbers {

    private static final int REPETITIONS = 9;

    public static int randomNum1(int a) {
        return (int) (Math.random() * a);
    }

    public static int randomNum2(int a) {
        for (int i = 0; i < REPETITIONS; i++) {
            int result = (int) (Math.random() * a);
            System.out.println(result);
        }
        return a;
    }

    public static int randomNum3() {
        for (int i = 0; i < REPETITIONS; i++) {
            int result = (int) (Math.random() * 11);
            System.out.println(result);
        }
        return 0;
    }

    public static int randomNum4() {
        for (int i = 0; i < REPETITIONS; i++) {
            int result = (int) (20 + (Math.random() * 51));
            System.out.println(result);
        }
        return 33;

    }

    public static int randomNum5() {
        for (int i = 0; i < REPETITIONS; i++) {
            int result = (int) (Math.random() * 20 - 10);
            System.out.println(result);
        }
        return -1;
    }

    public static int randomNum6() {
        for (int i = 0; i < (int) (3 + (Math.random() * 15)); i++) {
            int result = (int) (Math.random() * 45 - 10);
            System.out.println(result);
        }
        return 11;
    }
}
