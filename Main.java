package com.RandomNumbers;

public class Main {

    public static void main(String[] args) {
        System.out.println("-----------------------------------------TASK 1----------------------------------------------------------------------------------------");
        System.out.println(RandomNumbers.randomNum1(10));
        System.out.println("-----------------------------------------TASK 2----------------------------------------------------------------------------------------");
        System.out.println(RandomNumbers.randomNum2(100));
        System.out.println("-----------------------------------------TASK 3----------------------------------------------------------------------------------------");
        System.out.println(RandomNumbers.randomNum3());
        System.out.println("-----------------------------------------TASK 4----------------------------------------------------------------------------------------");
        System.out.println(RandomNumbers.randomNum4());
        System.out.println("-----------------------------------------TASK 5----------------------------------------------------------------------------------------");
        System.out.println(RandomNumbers.randomNum5());
        System.out.println("-----------------------------------------TASK 6----------------------------------------------------------------------------------------");
        System.out.println(RandomNumbers.randomNum6());
    }
}
